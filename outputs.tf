output "kubemaster_public_ip" {
  description = "Public IP address of the EC2 instance"
  value       = [aws_instance.kubemaster.public_ip]
}

output "private_ip" {
  description = "Private IP addresses of the EC2 instance"
  value       = [aws_instance.kubemaster.private_ip]
}

output "s3_bucket_name" {
    description = "Name of generated s3 bucket"
    value       = [random_string.bucket_name.result]
}