installTerraform:
	mkdir tmp
	wget https://releases.hashicorp.com/terraform/0.15.0/terraform_0.15.0_linux_amd64.zip -P tmp/
	unzip tmp/terraform_0.15.0_linux_amd64.zip -d tmp/
	sudo mv tmp/terraform /usr/bin/
	rm -rf tmp

prepare: installTerraform
	aws configure

init:
	terraform init

apply:
	terraform plan
	terraform apply

kubeconf:
	scripts/getKubeConf/getKubeConf.sh