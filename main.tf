terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "3.36.0"
    }
  }
}

provider "aws" {
  profile = "default"
  region  = var.default_az
}

resource "random_string" "bucket_name" {
  length  = 8
  upper   = false
  lower   = true
  number  = false
  special = false
}

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

data "template_file" "node" {
  template = file("cloud-init/node.yaml")

  vars = {
    aws_access_key_id = "${file("../secrets/aws_access_key_id")}"
    aws_secrets_access_key = "${file("../secrets/aws_secrets_access_key")}"
    aws_s3_bucket = random_string.bucket_name.result
  }
}

data "template_file" "kubemaster" {
  template = file("cloud-init/kubemaster.yaml")

    vars = {
    aws_access_key_id = "${file("../secrets/aws_access_key_id")}"
    aws_secrets_access_key = "${file("../secrets/aws_secrets_access_key")}"
    aws_s3_bucket = random_string.bucket_name.result
  }
}

resource "aws_security_group" "allow_ssh" {
  name = "allow_ssh"
  description = "Allow SSH inbound traffic"
  vpc_id      = aws_vpc.main.id

  ingress {
    description = "Everything from VPC"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "SSH from everywhere"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "HTTP from everywhere"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  
  ingress {
    description = "HTTP from everywhere"
    from_port   = 6443
    to_port     = 6443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "allow_ssh"
  }
}

resource "aws_security_group" "kubecluster" {
  name = "kubecluster"
  description = "Allow evrything from VPC"
  vpc_id      = aws_vpc.main.id

  ingress {
    description = "Everything from everywhere"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  
  ingress {
    description = "SSH from everywhere"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "kubecluster"
  }
}

resource "aws_vpc" "main"{
  cidr_block        = var.default_cidr_block
  instance_tenancy  = "default"
  enable_dns_hostnames = true
}

resource "aws_internet_gateway" "main" {
  vpc_id = aws_vpc.main.id
}

resource "aws_subnet" "main" {
  vpc_id            = aws_vpc.main.id
  cidr_block        = var.default_cidr_block
  availability_zone = "eu-central-1a"
}

resource "aws_route_table" "main" {
  vpc_id = aws_vpc.main.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.main.id
  }
}

resource "aws_route_table_association" "main" {
  subnet_id = aws_subnet.main.id
  route_table_id = aws_route_table.main.id
}

resource "aws_route53_record" "kubemaster" {
  zone_id = var.default_route_53_id
  name    = "kubemaster"
  type    = "A"
  ttl     = "300"
  allow_overwrite = true
  records = [aws_instance.kubemaster.public_ip]
}

resource "aws_s3_bucket" "kubeconfig" {
  bucket = "${random_string.bucket_name.result}"
  acl    = "private"
  force_destroy = "true"

  tags = {
    Name        = "My bucket"
    Environment = "chad"
  }
}

resource "aws_instance" "kubemaster" {
  ami                         = data.aws_ami.ubuntu.id
  instance_type               = "t3.small"
  subnet_id                   = aws_subnet.main.id
  vpc_security_group_ids      = [aws_security_group.allow_ssh.id]
  associate_public_ip_address = true
  key_name                    = "kubemaster"
  user_data                   = data.template_file.kubemaster.rendered
  tags = {
    Name = "kubemaster"
  }
}

resource "aws_launch_configuration" "launch-configuration" {
  name_prefix = "kubecluster_config-"
  image_id = data.aws_ami.ubuntu.id
  instance_type = "t2.micro"
  key_name = "node"
  associate_public_ip_address = true
  security_groups = [aws_security_group.kubecluster.id]
  user_data = data.template_file.node.rendered
}

resource "aws_autoscaling_group" "kubecluster" {
  name               = "kubecluster"
  max_size           = 3
  min_size           = 1
  launch_configuration = aws_launch_configuration.launch-configuration.name
  vpc_zone_identifier = [aws_subnet.main.id]

  instance_refresh {
    strategy = "Rolling"
    preferences {
      min_healthy_percentage = 50
    }
    triggers = ["tag"]
  }
  
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_policy" "kubecluster_policy" {
  name                   = "kubecluster_policy"
  scaling_adjustment     = 4
  adjustment_type        = "ChangeInCapacity"
  cooldown               = 300
  autoscaling_group_name = aws_autoscaling_group.kubecluster.name
}

resource "aws_cloudwatch_metric_alarm" "kubecluster_alarm" {
  alarm_name          = "kubecluster_alarm"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "2"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/EC2"
  period              = "120"
  statistic           = "Average"
  threshold           = "80"

  dimensions = {
    AutoScalingGroupName = aws_autoscaling_group.kubecluster.name
  }

  alarm_description = "This metric monitors kubecluster cpu utilization"
  alarm_actions     = [aws_autoscaling_policy.kubecluster_policy.arn]
}