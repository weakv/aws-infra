variable "default_cidr_block"{
    description = "Basic CIDR block for 256 hosts"
    default = "10.0.0.0/16"
}

variable "default_az"{
    description = "Default availability zone"
    default = "eu-central-1"
}

variable "default_route_53_id"{
    description = "ID of default route 53 hosted zone"
    default = "Z4OBNDPGWDT6X"
}