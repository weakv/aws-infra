import boto3
import argparse
import os
import time

def main(args):
    s3 = boto3.resource('s3')
    result = None
    
    print('Fetching kubeconfig file from S3 storage...')
    
    while result is None:
        if not os.path.isfile(args.file_name):
            try:
                result = s3.Bucket(args.bucket_name).download_file(args.object_name, args.file_name)
            except:
                print('File is not on S3 bucket, trying again in 10 seconds...')
            time.sleep(10)
        else:
            result = 0

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Fetches kubeconfig file from S3 storage')
    parser.add_argument('-b', dest='bucket_name', action='store')
    parser.add_argument('-o', dest='object_name', action='store')
    parser.add_argument('-f', dest='file_name', action='store')
    args = parser.parse_args()
    main(args)