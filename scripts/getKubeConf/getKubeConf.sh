#!/bin/bash
S3_BUCKET=$(terraform output -json | jq -r '.s3_bucket_name.value' | sed  's/"//g' | sed  's/[][]//g' )

mkdir -p $HOME/.config
cd scripts/getKubeConf && python3 -m pipenv install
pipenv run python3 ./getKubeConf.py -b $S3_BUCKET -o "admin.conf" -f $HOME/.kube/config